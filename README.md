# Hi!

I'm Dan.

### Tools & technologies
![](https://img.shields.io/static/v1?label=OS&message=fedora&color=blue&style=for-the-badge&logo=fedora)<br/>
![](https://img.shields.io/static/v1?label=ide&message=PyCharm&color=blue&style=for-the-badge&logo=pycharm)<br/>
![](https://img.shields.io/static/v1?label=code&message=python&color=blue&style=for-the-badge&logo=python)<br/>
![](https://img.shields.io/static/v1?label=tools&message=gitlab&color=blue&style=for-the-badge&logo=gitlab)
![](https://img.shields.io/static/v1?label=tools&message=github&color=blue&style=for-the-badge&logo=github)
### Current projects

